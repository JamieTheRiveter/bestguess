#!/bin/bash

SHOWOUTPUT=

prog=../bestreport

printf "%s\n"   '----------------------------------------------'
printf "%s\n"   "$0"
printf "%s\n"   'Bestreport tests (scale)'
printf "%s\n"   'See test-scale.sh for smaller/shorter tests'
printf "%s\n\n" '----------------------------------------------'

declare -a output
allpassed=1

source ./common.sh

#
# Large dynamic range (max diff between observations)
#

echo dynrange29.csv
ok "$prog" -QE dynrange29.csv
contains "0 time" "0 μs"
contains "2^29 time" "536.87 s" 
contains "Slower" "536.87 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
missing "precision"

echo dynrange30-1.csv
ok "$prog" -QE dynrange30-1.csv
contains "0 time" "0 μs"
contains "2^30-1 time" "1073.74 s"
contains "Slower" "1073.74 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
missing "precision"

echo dynrange30.csv
ok "$prog" -QE dynrange30.csv
contains "0 time" "0 μs"
contains "2^30 time" "1073.74 s"
contains "Slower" "1073.74 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
missing "precision"

echo dynrange31-1.csv
ok "$prog" -QE dynrange31-1.csv
contains "0 time" "0 μs"
contains "2^31-1 time" "2147.48 s" 
contains "Slower" "2147.48 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
missing "1 bit of precision"

echo dynrange31.csv
ok "$prog" -QE dynrange31.csv
contains "0 time" "0 μs"
contains "2^31 time" "2147.48 s" 
contains "Slower" "2147.48 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
contains "1 bit of precision"

echo dynrange32-1.csv
ok "$prog" -QE dynrange32-1.csv
contains "0 time" "0 μs"
contains "2^32-1 time" "4294.97 s"
contains "Slower" "4294.97 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
contains "1 bit of precision"

echo dynrange32.csv
ok "$prog" -QE dynrange32.csv
contains "0 time" "0 μs"
contains "2^32 time" "4294.97 s"
contains "Slower" "4294.97 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
contains "2 bits of precision"

echo dynrange33.csv
ok "$prog" -QE dynrange33.csv
contains "0 time" "0 μs"
contains "2^33 time" "8589.93 s"
contains "Slower" "8589.93 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
contains "Warning"
contains "Largest differences between observations exceeds"
contains "Ranking calculation will be slow."
contains "3 bits of precision"

#
# Large number of observations
#

echo 50k.csv
ok "$prog" -QE 50k.csv
contains "/usr/bin/true" "1.99 ms"
contains "/bin/ls" "2.09 ms"
contains "Slower" "0.10 ms   1.05x"
contains "N = 50000" "U = 21687569"
contains "p < 0.001  ( n/a )"
contains "Δ = 0.10 ms"
contains "Â = 0.01"
contains "Warning" "Number of sample differences is large"

echo 50k-0.7.3.csv
ok "$prog" -QE 50k-0.7.3.csv 
contains "/usr/bin/true" "1.97 ms"
contains "ps Aux" "30.73 ms"
contains "Slower" "28.76 ms  15.59x"
contains "N = 50000" "U = 0"
contains "p < 0.001  ( n/a )"
contains "Δ = 28.76 ms"
contains "Â = 0.00"
contains "Warning" "Number of sample differences is large"

echo 100k.csv
ok "$prog" -QE 100k.csv
contains "/usr/bin/true" "1.96 ms"
contains "/bin/ls -l" "2.52 ms"
contains "Slower" "0.56 ms   1.28x"
contains "N = 100000" "U = 5574660"
contains "p < 0.001  ( n/a )"
contains "Δ = 0.51 ms"
contains "Â = 0.00"
contains "Warning" "Number of sample differences is large"

echo 100k-0.7.3.csv
ok "$prog" -QE 100k-0.7.3.csv
contains "/usr/bin/true" "1.97 ms"
contains "ps Aux" "30.50 ms"
contains "Slower" "28.53 ms  15.45x"
contains "N = 100000" "U = 0"
contains "p < 0.001  ( n/a )"
contains "Δ = 28.19 ms"
contains "Â = 0.00"
contains "Warning" "Number of sample differences is large"

echo 250k-0.7.3.csv
ok "$prog" -QE 250k-0.7.3.csv
contains "/usr/bin/true" "1.99 ms"
contains "ps Aux" "30.21 ms"
contains "Slower" "28.21 ms  15.16x"
contains "N = 250000" "U = 0"
contains "p < 0.001  ( n/a )"
contains "Δ = 27.23 ms"
contains "Â = 0.00"
contains "Warning" "Number of sample differences is large"

#
# -----------------------------------------------------------------------------
#

if [[ "$allpassed" == "1" ]]; then 
    printf "All tests passed.\n\n"
    exit 0
else
    exit -1
fi


