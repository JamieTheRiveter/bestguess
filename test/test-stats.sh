#!/bin/bash

SHOWOUTPUT=

prog=../bestreport

printf "%s\n"   '-------------------------------------'
printf "%s\n"   "$0"
printf "%s\n"   'Bestreport tests (stats calculations)'
printf "%s\n\n" '-------------------------------------'

declare -a output
allpassed=1

set -e
source ./common.sh

# https://www.statsdirect.co.uk/help/nonparametric_methods/mann_whitney.htm
#
#   Observations (x) in Farm Boys = 12 median = 9.8 rank sum = 321
#   Observations (y) in Town Boys = 36 median = 7.75
#
#   U = 243 U' = 189
#   Theta = 0.437500 (95% CI: 0.271772 to 0.621223)
#
#   Exact probability (adjusted for ties):
#   Lower side P = 0.2645 (H1: x tends to be less than y)
#   Upper side P = 0.7355 (H1: x tends to be greater than y)
#   Two sided P = 0.529 (H1: x tends to be distributed differently to y)
#
#   95.1% confidence interval for difference between medians or means:
#   K = 134 median difference = 0.8
#   CI = -2.3 to 4.4

ok "$prog" -c alpha=0.05 -QE farmboys.csv
contains "town" "7.75 ms" "farm" "9.80 ms"
contains "Slower" "2.05 ms" "1.26x"
contains "N = 12" "U = 189"
contains "p = 0.528" "95.05%" "(-2.30, 4.40)"
contains "Δ = 0.80 ms"
contains "Â = 0.43"


# https://statisticsbyjim.com/hypothesis-testing/mann-whitney-u-test/
# Results are shown there in an image, instead of as copyable text.
#
# Note: When looking for a C.I. near 95%, BestGuess is choosing the
# lower of the confidence intervals that span 95%, thus opting for a
# narrower interval (with less confidence).  The statisticsbyjim
# article chooses the 95.5% interval with its wider interval.
#
# Article gives W without subtracting out the minimum possible rank
# sum.  Their U = 76 is (n1 * (n1+1) / 2) = 66 higher than ours.

ok "$prog" -c alpha=0.05 -QE brandAB.csv
contains "cmd1" "36.00 ms" "cmd2" "37.60 ms" "Slower" "1.85 ms"
contains "N = 10" "U = 10"
contains "p = 0.002" "(< 0.001)"
contains "Δ = 1.85 ms"
contains "94.72%" "(1.00, 2.80) ms"
contains "Â = 0.09"

# https://rcompanion.org/handbook/F_04.html
#
# The W reported on this site is the one with the minimum possible
# rank sum (n1 * (n1+1) / 2) subtracted out, like ours.
#
# wilcox.exact(Likert ~ Speaker, data=Data, exact=TRUE)
#   Exact Wilcoxon rank sum test
#   U = 5, p-value = 0.0002382

ok "$prog" -c alpha=0.05 -QE poohpiglet.csv
contains "2: Piglet" "2.00 ms" "1: Pooh" "4.00 ms" "Slower" "2.00x"
contains "N = 10" "U = 5"
contains "p < 0.001" "n/a"
contains "Δ = 2.00 ms"
contains "98.74%" "(2.00, 3.00) ms"
contains "Â = 0.01"

#
# -----------------------------------------------------------------------------
#

if [[ "$allpassed" == "1" ]]; then 
    printf "All tests passed.\n\n"
    exit 0
else
    exit -1
fi


