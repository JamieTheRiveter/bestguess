#!/bin/bash

if [[ ! -x $(command -v "stackcollapse.pl") ]]; then
    echo "Flamegraph scripts not installed, or not in PATH"
    exit 1
fi;

command=$1
command_line="$*"

if [[ "$1" == "" ]]; then
    echo "Usage: $0 <command> [arg0] [arg1] ..."
    exit 1
fi;

rm -f out.stacks

echo "Running command..."

#sudo dtrace "-x" "ustackframes=100" "-n" 'profile-997 /pid == $target/ { @[ustack(100)] = count(); }' "-o" "out.stacks" "-c" "../bestguess -r 1000 ls"

# sudo dtrace -x ustackframes=100 \
#      -n 'profile-5000 /execname == "$command" && arg1/ { @[ustack()] = count(); }' \
#      -o out.stacks \
#      -c "$command_line"

sudo dtrace "-x" "ustackframes=100" "-n" 'profile-5000 /pid == $target/ { @[ustack(100)] = count(); }' "-o" "out.stacks" "-c" "../bestreport 8k.csv"

sudo dtrace -x ustackframes=100 \
     -n 'profile-5000 /pid == $target/ { @[ustack()] = count(); }' \
     -o "out.stacks" \
     -c "$command_line"

echo "Folding stack frames..."
stackcollapse.pl out.stacks > out.folded
echo "Producing flamegraph..."
flamegraph.pl out.folded > out.svg
echo "Wrote out.svg"



