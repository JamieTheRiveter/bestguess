#!/bin/bash

SHOWOUTPUT=

prog=../bestreport

printf "%s\n"   '----------------------------------------------'
printf "%s\n"   "$0"
printf "%s\n"   'Bestreport tests (scale)'
printf "%s\n"   'See test-highscale.sh for larger/longer tests'
printf "%s\n\n" '----------------------------------------------'

declare -a output
allpassed=1

source ./common.sh

#
# Large dynamic range (max diff between observations)
#

ok "$prog" -QE dynrange24.csv
contains "0 time" "0 μs"
contains "2^24 time" "16.78 s"
contains "Slower" "16.78 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
missing "Warning"

ok "$prog" -QE dynrange26.csv
contains "0 time" "0 μs"
contains "2^26 time" "67.11 s"
contains "Slower" "67.11 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
missing "Warning"

ok "$prog" -QE dynrange28.csv
contains "0 time" "0 μs"
contains "2^28 time" "268.44 s" 
contains "Slower" "268.44 s  n/a"
contains "p = 0.047  ( n/a )"
contains "Â = 0.00"
missing "Warning"

#
# Large number of observations
#

ok "$prog" -QE 25k.csv
contains "/usr/bin/true" "2.01 ms"
contains "/bin/ls -l" "2.54 ms"
contains "Slower" "0.54 ms   1.27x"
contains "N = 25000" "U = 12758347"
contains "p < 0.001  ( n/a )"
contains "Δ = 0.53 ms"
contains "Â = 0.02"
missing "Warning"

#
# -----------------------------------------------------------------------------
#

if [[ "$allpassed" == "1" ]]; then 
    printf "All tests passed.\n\n"
    exit 0
else
    exit -1
fi


