#!/bin/bash

prog="../bestguess"
report="../bestreport"

printf "%s\n"   '--------------------------------------'
printf "%s\n"   "$0"
printf "%s\n"   'Command file tests'
printf "%s\n\n" '--------------------------------------'

SHOWOUTPUT=

declare -a output
allpassed=1

source ./common.sh

#
# 
#

runtime "$prog" -f thisfiledoesnotexist
contains "No such file or directory"

ok "$prog" -QR -f pi.txt
contains "Lacking the 5 timed runs to statistically rank"
contains "pi to 2500 digits"
contains "pi to 3500 digits"
contains "pi to 2000 digits"
contains "pi to 3000 digits"
contains "pi to 4000 digits"
missing "Warning"




#
# -----------------------------------------------------------------------------
#

if [[ "$allpassed" == "1" ]]; then 
    printf "All tests passed.\n\n"
    exit 0
else
    exit -1
fi


