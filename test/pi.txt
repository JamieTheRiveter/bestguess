#
# These commands are written to run in a shell
#
-s "/bin/bash -c"
#
# On macos, can use 'bc -le', but not on Linux
#
bc -l <<<"scale=2000;4*a(1)" >/dev/null
-n "pi to 2000 digits"
bc -l <<<"scale=2500;4*a(1)" >/dev/null
-n "pi to 2500 digits"
bc -l <<<"scale=3000;4*a(1)" >/dev/null
-n "pi to 3000 digits"
bc -l <<<"scale=3500;4*a(1)" >/dev/null
-n "pi to 3500 digits"
bc -l <<<"scale=4000;4*a(1)" >/dev/null
-n "pi to 4000 digits"
