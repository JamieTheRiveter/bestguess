//  -*- Mode: C; -*-                                                       
// 
//  stats.h  Summary statistics
// 
//  COPYRIGHT (c) Jamie A. Jennings, 2024

#ifndef stats_h
#define stats_h

#include "bestguess.h"
#include "utils.h"
#include <sys/resource.h>

// Min, max, and measures of central tendency
typedef struct Measures {
  int64_t min;
  int64_t max;
  int64_t mode;
  int64_t median;
  int64_t pct95;
  int64_t pct99;
  int64_t Q1;
  int64_t Q3;
  double  est_mean;	// Estimated
  double  est_stddev;	// Estimated
  double  ADscore;	// ** See below **
  double  p_normal;	// p-value of ADscore
  double  skew;		// Non-parametric skew
  double  kurtosis;	// Distribution shape compared to normal
  uint8_t code;		// Info about ADscore and skew
} Measures;

// Inferential statistics comparing a sample to a reference sample
// (like the fastest performer)
typedef struct Inference {
  uint8_t indistinct;	// See below
  double  U;		// Mann-Whitney-Wilcoxon, min(U1, U2)
  double  p;		// p value that η₁ ≠ η₂ (conservative)
  double  p_adj;	// p value adjusted for ties (or -1 if too many ties)
  double  shift;	// Hodges-Lehmann estimation of median shift (μs)
  double  confidence;	// Confidence for shift, e.g. 0.955 for 95.5%
  int64_t ci_low;	// Confidence interval for shift (μs)
  int64_t ci_high;	// (μs)
  double  p_super;	// Probability of superiority
} Inference;

// Flags for 'indistinct' in Inference (results of evaluating
// inferential statistics).  When 'indistinct' is zero, we have a
// significant, sizable difference between two samples.
enum InferenceFlags {
  INF_NONSIG,		// p value not significant
  INF_CIZERO,		// CI includes zero
  INF_NOEFFECT,		// Effect size (diff) too small
  INF_HIGHSUPER,	// Prob of superiority too high
};

// ** IMPORTANT **
//
// The 'ADscore' field in Measures holds the Anderson-Darling
// normality (distance) ONLY if these flags are all FALSE:
// CODE_HIGHZ, CODE_SMALLN, CODE_LOWVARIANCE
//
// When the flag CODE_HIGHZ is true, 'ADscore' holds the highest
// Z-score we found.

// Statistical summary of a set of runs of a single command
typedef struct Summary {
  char      *cmd;		// Never NULL (can be epsilon)
  char      *shell;		// Never NULL (can be epsilon)
  char      *name;		// Optional, can be NULL
  int        batch;		// Unique (within a single CSV file) ID
  int        runs;
  int        fail_count;
  Measures   user;
  Measures   system;
  Measures   total;
  Measures   maxrss;
  Measures   vcsw;
  Measures   icsw;
  Measures   tcsw;
  Measures   wall;
  Inference *infer;		// Can be NULL
} Summary;

// Bitmasks
#define HAS(byte, flagname) (((uint8_t)1)<<(flagname) & byte)
#define SET(byte, flagname) do {		\
    byte = (byte) | (((uint8_t)1)<<(flagname));	\
  } while (0)

// Flags for 'code' in Measures
enum MeasureCodes {
  CODE_HIGHZ,		  // Very high Z scores ==> No AD score 
  CODE_SMALLN,		  // Insufficient observations ==> No AD score
  CODE_LOWVARIANCE,	  // Very low variance ==> No AD score
  CODE_HIGH_SKEW,	  // High skew can explain non-normality
  CODE_HIGH_KURTOSIS,	  // High kurtosis can explain non-normality
};

typedef struct Ranking {
  Usage *usage;	       // All the usage data, collected together
  Summary **summaries; // Array of pointers to summaries
  int64_t *index;      // summary[index[0]] is fastest
  int64_t *usageidx;   // usage[usageidx[i],usageidx[i+1]] ==> summary[i]
  int64_t count;       // Number of summaries, index, usageidx
} Ranking;

// -----------------------------------------------------------------------------

// The minimum sample size needed for our inferential statistics is
// somewhere around n₁ = n₂ = 3.  Complete separation between the
// samples would be needed in order to conclude η₁ ≠ η₂ with samples
// that small.  If we require each sample to have at least 5
// observations, then complete separation should produce a p value
// around 0.016, so we could conceivably see that p value empirically.
//
// (/ 2.0 (choose 10 5)) ==>  0.00793650 ≈ 0.008

#define INFERENCE_N_THRESHOLD 5

Inference *compare_samples(Usage *usage,
			   double alpha,
			   int64_t ref_start, int64_t ref_end,
			   int64_t idx_start, int64_t idx_end);

int64_t *sort_by_totaltime(Summary *summaries[], int64_t start, int64_t end);

Summary *summarize(Usage *usage, int64_t start, int64_t end);
void     free_summary(Summary *s);
void     free_summaries(Summary **ss, int64_t n);

Ranking *rank(Usage *usage);
void     free_ranking(Ranking *rank);

#endif
