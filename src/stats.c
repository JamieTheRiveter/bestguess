//  -*- Mode: C; -*-                                                       
// 
//  stats.c  Summary statistics
// 
//  Copyright (C) Jamie A. Jennings, 2024

#include "bestguess.h"
#include "utils.h"
#include "stats.h"
#include <math.h>
#include <assert.h>

/* 
   Note: For skewed distributions, the median is a more useful measure
   of the "typical" value than is the arithmetic mean. 

   But if we had to choose a single number to characterize a
   distribution of runtimes, the mode (or modes) may be the most
   useful as it represents the most common value.  That is, the mode
   is the most "typical" runtime.

   However, if we are more concerned with the long tail, then the 95th
   and 99th percentile values should be highlighted.

   Given a choice between either median or mean, the median of a
   right-skewed distribution is typically the closer of the two to the
   mode.

 */

// -----------------------------------------------------------------------------
// Z score calculation without a table
// -----------------------------------------------------------------------------

// Cumulative (less than Z) Z-score calculation.  Instead of a table,
// we iteratively estimate each needed value to an accuracy greater
// than that of most tables.
//
// Benefits:
//
// (1) Tables are only accurate to 5 digits, and we can get
// around 15 digits from Phi(x) below.
//
// (2) Tables usually provide values for Z-scores from about -4.0 to
// 4.0, and we can get fairly accurate results out to -8.0 and 8.0.
//
// Phi and cPhi functions are published here:
//
//   Marsaglia, G. (2004). Evaluating the Normal Distribution.
//   Journal of Statistical Software, 11(4), 1–11.
//   https://doi.org/10.18637/jss.v011.i04
//
// Phi(x) produces "an absolute error less than 8×10^−16."  Marsaglia
// suggests returning 0 for x < −8 and 1 for x > 8 "since an error of
// 10^−16 can make a true result near 0 negative, or near 1, exceed 1."
//
static double Phi(double x) {
  if (x != x) PANIC("Argument not a number");
  if (x < -8.0) return 0.0;
  if (x > 8.0) return 1.0;
  long double s = x, t = 0.0, b = x, q = x*x, i = 1.0;
  while (s != t)
    s = (t=s) + (b *= q / (i += 2));
  return 0.5 + s * exp(-0.5 * q - 0.91893853320467274178L);
}

__attribute__((unused))
static double cPhi(double x) {
  if (x != x) PANIC("Argument not a number");
  int j = 0.5 * (fabs(x) + 1.0);
  if (j >= 9) return 0.0;
  long double R[9] = { 1.25331413731550025L,
		       0.421369229288054473L,
		       0.236652382913560671L,
		       0.162377660896867462L,
		       0.123131963257932296L,
		       0.0990285964717319214L,
		       0.0827662865013691773L,
		       0.0710695805388521071L,
		       0.0622586659950261958L };
  long double pwr = 1.0, a = R[j], z = 2*j, b = a*z - 1;
  long double h = fabs(x) - z, s = a + h*b, t = a, q = h * h;
  for (int i = 2; s != t; i += 2){
    a = (a + z*b)/i;
    b = (b+ z*a)/(i+1);
    pwr *= q;
    s = (t=s) + pwr*(a + h*b);
  }
  s = s * exp(-0.5*x*x - 0.91893853320467274178L);
  if (x >= 0)
    return (double) s;
  return (double) (1.0 - s);
}

// The CDF of the normal function increases monotonically, so we can
// numerically invert it using binary search.  It's not fast, but we
// don't need it to be fast.
static double invPhi(double p) {
  if (p <= 0.0) return -8.0;
  if (p >= 1.0) return  8.0;
  double Zhigh = (p < 0.5) ? 0.0 : 8.0;
  double Zlow = (p < 0.5) ? -8.0 : 0.0;
  double Zmid, approx, err;
  while (1) {
    Zmid = Zlow + (Zhigh - Zlow) / 2.0;
    approx = Phi(Zmid);
    err = approx - p;
    if (fabs(err) < 0.000001)	// Adjust as necessary
      return Zmid;
    if (err > 0.0) 
      Zhigh = Zmid;
    else
      Zlow = Zmid;
  }
}

// -----------------------------------------------------------------------------
// Testing a sample distribution for normality
// -----------------------------------------------------------------------------

// https://en.wikipedia.org/wiki/Anderson–Darling_test
//
// Input: Xi are samples, order from smallest to largest
//        n is the number of samples
//        i ranges from 1..n inclusive
//
// Estimate mean of sample distribution: μ = (1/n) Σ(Xi)
// Estimate variance: σ² = (1/(n-1)) Σ(Xi-μ)²
//
// Standardize Xi as: Yi = (Xi-μ)/σ
//
// Compute F(Yi) where F is the CDF for (in this case) the normal
// distribution, and is the Z-score for each Yi. Let Zi = F(Yi).
//
// To compare our distribution (the samples Xi) to a normal
// distribution, we compute the distance A² as follows:
//
// A² = -n - (1/n) Σ( (2i-1)ln(Zi) + (2(n-i)+1)ln(1-Zi) )
//
// When neither the mean or variance of the sample distribution are
// known, a correction is applied to give a final distance measurement
// of: A⃰² = A²(1 + 4/n + 25/n²)
// 
// Or this alternative: A⃰² = A²(1 + 0.75/n + 2.25/n²)
//
// We'll the alternative correction, and the term 'AD score' to mean
// the quantity A⃰².
//
// The hypothesis that our sample distribution is normal is rejected
// if the AD score.  Wikipedia cites the book below for the
// alternative correction mentioned above, and these critical values:
// 
//         p value  0.10   0.05   0.025   0.01  0.005
//  critical value  0.631  0.754  0.884  1.047  1.159
//
// Ralph B. D'Agostino (1986). "Tests for the Normal Distribution". In
// D'Agostino, R.B.; Stephens, M.A. (eds.). Goodness-of-Fit
// Techniques. New York: Marcel Dekker. ISBN 0-8247-7487-6.
//
// The calculate_p() function produces the needed critical values:
//                   p value  0.10    0.05    0.025   0.01    0.005
// calculated critical value  0.1001  0.0498  0.0238  0.0094  0.0050

// FWIW:
//
// https://support.minitab.com/en-us/minitab/help-and-how-to/statistics
// /basic-statistics/supporting-topics/normality/test-for-normality/
//
// "Anderson-Darling tends to be more effective in detecting
// departures in the tails of the distribution. Usually, if departure
// from normality at the tails is the major problem, many
// statisticians would use Anderson-Darling as the first choice."

//
// Calculating p-value for normal distribution based on AD score.  See
// e.g. https://real-statistics.com/non-parametric-tests/goodness-of-fit-tests/anderson-darling-test/
// or https://www.statisticshowto.com/anderson-darling-test/
//
// The null hypothesis is that the sample distribution is normal.  A
// low p-value is a high likelihood that the distribution is NOT
// normal.  E.g. p = 0.01 indicates a 1% chance that the distribution
// we examined is actually normal, or a 99% chance it is NOT normal.
//
static double calculate_p(double AD) {
  if (AD <= 0.20) return 1.0 - exp(-13.436 + 101.14*AD - 223.73*AD*AD);
  if (AD <= 0.34) return 1.0 - exp(-8.318 + 42.796*AD - 59.938*AD*AD);
  if (AD <  0.60) return exp(0.9177 - 4.279*AD - 1.38*AD*AD);
  else return exp(1.2937 - 5.709*AD + 0.0186*AD*AD);
}

//
// Wikipedia cites the book below for the claim that a minimum of 8
// data points is needed.
// https://en.wikipedia.org/wiki/Anderson–Darling_test#cite_note-RBD86-6
// 
// Ralph B. D'Agostino (1986). "Tests for the Normal Distribution". In
// D'Agostino, R.B.; Stephens, M.A. (eds.). Goodness-of-Fit
// Techniques. New York: Marcel Dekker. ISBN 0-8247-7487-6.
//
static double AD_from_Y(int64_t n,	      // number of data points
			double *Y,	      // standardized ranked data
			double (F)(double)) { // CDF function
  assert(Y && (n > 0));
  double S = 0;
  for (int64_t i = 1; i <= n; i++)
    S += (2*i-1) * log(F(Y[i-1])) + (2*(n-i)+1) * log(1.0-F(Y[i-1]));
  S = S/n;
  double A = -n - S;
  // Recommended correction factor for our critical p-values
  A = A * (1 + 0.75/n + 2.25/(n * n));
  return A;
}

// Returns true if we were able to calculate an AD score, and false if
// we encountered Z scores too high to do the calculation.  In the
// latter case, the 'ADscore' value is the most extreme Z score seen.
// The returned boolean should be saved as CODE_HIGHZ so that the
// meaning of 'ADscore' can be properly interpreted.
//
static bool AD_normality(int64_t *X,    // ranked data points
			 int64_t n,	// number of data points
			 double mean,
			 double stddev,
			 double *ADscore) {
  assert(X && (n > 0));
  if (!X || !ADscore) PANIC_NULL();

  double *Y = malloc(n * sizeof(double));
  if (!Y) PANIC_OOM();

  // Set zero value for 'extremeZ' as a sentinel
  double extremeZ = 0.0;

  for (int64_t i = 0; i < n; i++) {
    // Y is a vector of studentized residuals
    Y[i] = ((double) X[i] - mean) / stddev;
    if (Y[i] != Y[i]) {
      PANIC("Got NaN.  Should not have attempted AD test because stddev is zero.");
    }
    // Extreme values (i.e. high Z scores) indicate a long tail, and
    // prevent the computation of a meaningful AD score.  NOTE: An
    // observation that is around 7 std deviations from the mean will
    // occur in a normal distribution with probability 1 in 390 BILLION.
    //
    // See e.g. https://en.wikipedia.org/wiki/68–95–99.7_rule
    //
    if (fabs(Y[i]) > 7.0) {
      // Track the highest Z score we encounter, so we can report it
      if (fabs(Y[i]) > fabs(extremeZ))
	extremeZ = Y[i];
    }
  }
  *ADscore = (extremeZ != 0.0) ? extremeZ : AD_from_Y(n, Y, Phi);
  free(Y);
  return (extremeZ == 0.0);
}

// -----------------------------------------------------------------------------
// Attributes of the distribution that we can calculate directly
// -----------------------------------------------------------------------------

static int64_t avg(int64_t a, int64_t b) {
  return (a + b) / 2;
}

#define VALUEAT(i) (X[i])
#define WIDTH(i, j) (VALUEAT(j) - VALUEAT(i))

// "Half sample" technique for mode estimation.  The base cases are
// when number of samples, 𝑛, is 1, 2, or 3.  When 𝑛 > 3, there is a
// recursive case that computes the mode of ℎ = 𝑛/2 samples.  To
// choose which 𝑛/2 samples, we examine every interval [i, i+ℎ) to
// find the one with the smallest width.  That sequence of samples is
// the argument to the recursive call.
//
// See https://arxiv.org/abs/math/0505419
//
// Note that the data, X, must be sorted.
//
static int64_t estimate_mode(int64_t *X, int64_t n) {
  assert(X && (n > 0));
  // n = size of sample being examined (number of observations)
  // h = size of "half sample" (floor of n/2)
  // idx = start index of samples being examined
  // limit = end index one beyond last sample being examined
  int64_t wmin = 0;
  int64_t idx = 0;
  int64_t limit, h;

 tailcall:
  if (n == 1) {
    return VALUEAT(idx);
  } else if (n == 2) {
    return avg(VALUEAT(idx), VALUEAT(idx+1));
  } else if (n == 3) {
    // Break a tie in favor of lower value
    if (WIDTH(idx, idx+1) <= WIDTH(idx+1, idx+2))
      return avg(VALUEAT(idx), VALUEAT(idx+1));
    else
      return avg(VALUEAT(idx+1), VALUEAT(idx+2));
  }
  h = n / 2;	     // floor
  wmin = WIDTH(idx, idx+h);
  // Search for half sample with smallest width
  limit = idx+h;
  for (int64_t i = idx+1; i < limit; i++) {
    // Break ties in favor of lower value
    if (WIDTH(i, i+h) < wmin) {
      wmin = WIDTH(i, i+h);
      idx = i;
    }
  }
  n = h+1;
  goto tailcall;
}

// Returns -1 when there are insufficient observations for a 95th or
// 99th percentile request.  For quartiles, we make the best estimate
// we can with the data we have.
static int64_t percentile(int pct, int64_t *X, int64_t n) {
  if (!X) PANIC_NULL();
  if (n < 1) PANIC("No data on which to calculate a percentile");
  switch (pct) {
    case 0:
      return X[0];		// Q0 = Min
    case 25:
      return X[n/4];		// Q1
      break;
    case 50:
      if (n & 0x1)		// Q2 = Median
	return X[n/2];
      return avg(X[(n/2) - 1], X[n/2]);
    case 75:
      return X[(3*n)/4];	// Q3
    case 95:
      if (n < 20) return -1;
      return X[n - n/20];
    case 99:
      if (n < 100) return -1;
      return X[n - n/100];
      break;
    case 100:
      return X[n - 1];
    default:
      PANIC("Percentile %d unimplemented", pct);
  }
}

// Estimate the sample mean: μ = (1/n) Σ(Xi)
static double estimate_mean(int64_t *X, int64_t n) {
  assert(X && (n > 0));
  double sum = 0;
  for (int64_t i = 0; i < n; i++)
    sum += X[i];
  return sum / n;
}

// Estimate the sample variance: σ² = (1/(n-1)) Σ(Xi-μ)²
// Return σ, the estimated standard deviation.
static double estimate_stddev(int64_t *X, int64_t n, double est_mean) {
  assert(X && (n > 0));
  double sum = 0;
  for (int64_t i = 0; i < n; i++)
    sum += pow(X[i] - est_mean, 2);
  return sqrt(sum / (n - 1));
}

static int64_t *make_index(Usage *usage, int64_t start, int64_t end, Comparator compare) {
  assert(usage);
  int64_t runs = end - start;
  if (runs <= 0) PANIC("Invalid start/end");
  int64_t *index = malloc(runs * sizeof(int64_t));
  if (!index) PANIC_OOM();
  for (int64_t i = 0; i < runs; i++) index[i] = start + i;
  sort(index, runs, sizeof(int64_t), compare, usage);
  return index;
}

static int64_t *ranked_sample(Usage *usage,
			      int64_t start,
			      int64_t end,
			      FieldCode fc,
			      Comparator comparator) {
    int64_t *index = make_index(usage, start, end, comparator);
    int64_t runs = end - start;
    int64_t *X = malloc(runs * sizeof(double));
    if (!X) PANIC_OOM();
    for (int64_t i = 0; i < runs; i++)
      X[i] = get_int64(usage, index[i], fc);
    free(index);
    return X;
}

// When the AD test for normality says "not close to normal", we may
// want to know why.  The kurtosis can tell us if the shape is not
// normal in the sense of the peak and tails, and skewness can tell us
// about asymmetry.

// https://www.stat.cmu.edu/~hseltman/files/spssSkewKurtosis.R
// spssSkewKurtosis=function(x) {
//   w=length(x)
//   m1=mean(x)
//   m2=sum((x-m1)^2)
//   m3=sum((x-m1)^3)
//   m4=sum((x-m1)^4)
//   s1=sd(x)
//   skew=w*m3/(w-1)/(w-2)/s1^3
//   sdskew=sqrt( 6*w*(w-1) / ((w-2)*(w+1)*(w+3)) )
//   kurtosis=(w*(w+1)*m4 - 3*m2^2*(w-1)) / ((w-1)*(w-2)*(w-3)*s1^4)
//   sdkurtosis=sqrt( 4*(w^2-1) * sdskew^2 / ((w-3)*(w+5)) )
//   mat=matrix(c(skew,kurtosis, sdskew,sdkurtosis), 2,
//         dimnames=list(c("skew","kurtosis"), c("estimate","se")))
//   return(mat)
// }

// Excess kurtosis = (1 / n) * Σ((X_i - mean) / std)⁴ - 3
//   > 0 ==> "sharp peak, heavy tails"
//   < 0 ==> "flat peak, light tails"
//   near zero, the distribution resembles a normal one
static double kurtosis(int64_t *X,
		       int64_t n,
		       double mean,
		       double stddev) {
  double sum = 0.0;
  for (int64_t i = 0; i < n; i++)
    sum += pow((X[i] - mean) / stddev, 4.0);
  return (sum / n) - 3.0;
}

// Moment-based calculation of skew
// skew = (n / ((n - 1) * (n - 2))) * Σ((X_i - mean) / std)³
//   abs(skew) < 0.5 ==> approximately symmetric
//   0.5 < abs(skew) < 1.0 ==> moderately skewed
//   abs(skew) > 1.0 ==> highly skewed
static double skew(int64_t *X,
		   int64_t n,
		   double mean,
		   double stddev) {
  double sum = 0.0;
  for (int64_t i = 0; i < n; i++)
    sum += pow((X[i] - mean) / stddev, 3.0);
  return sum * n / (n-1) / (n-2);
}

// "As the standard errors get smaller when the sample size increases,
// z-tests under null hypothesis of normal distribution tend to be
// easily rejected in large samples"
// https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3591587/
//
// If we follow the guidance in that article, we should use absolute
// values of 2 (skew) and 4 (excess kurtosis) as thresholds of
// substantial deviation from normality for sample sizes > 300.
// 
// Sample sizes < 50
//   ==> Reject normality when Zskew or Zkurtosis > 1.96 (alpha = .05)
// Sample sizes 50..300
//   ==> Reject normality when Zskew or Zkurtosis > 3.29 (alpha = .001)
//
// Similar advice is given in
// https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3693611/
//
// Reminder:
//   α = .05 ==> critical Z score (abs) 1.96
//   α = .01 ==> critical Z score (abs) 2.58
//   α = .001 ==> critical Z score (abs) 3.29

static double Zcrit(double alpha) {
  return fabs(invPhi(alpha / 2.0));
}

static double skew_stddev(int64_t n) {
  return sqrt(6.0 * n * (n-1) / ((n-2) * (n+1) * (n+3)));
}

static double skew_kurtosis_Zcrit(int64_t n) {
  if (n > 100) return Zcrit(0.001);
  if (n > 50) return Zcrit(0.01);
  return Zcrit(0.05);
}

static bool nonnormal_skew(double skew, int64_t n) {
  static double skew_large_sample = 2.0;
  if (n > 300) return (fabs(skew) > skew_large_sample);
  double sdskew = skew_stddev(n);
  double Zabs = fabs(skew / sdskew);
  return (Zabs > skew_kurtosis_Zcrit(n));
}

static bool nonnormal_kurtosis(double kurtosis, int64_t n) {
  if (n > 300) return (fabs(kurtosis) > 4.0); // 7.0 - 3.0
  double sdskew = skew_stddev(n);
  double sdkurtosis = sqrt(4.0 * (n*n - 1) * sdskew * sdskew / ((n-3)*(n+5)));
  double Zabs = fabs(kurtosis / sdkurtosis);
  return (Zabs > skew_kurtosis_Zcrit(n));
}

// FUTURE: How should we set the threshold for a stddev (or variance)
// too small for the ADscore to be meaningful?

#define LOWMEAN_THRESHOLD 0.1	   // Good for integer number of microsecs
#define LOWSTDDEV_THRESHOLD 0.1    // Somewhat arbitrary
#define ADTEST_N_THRESHOLD 8	   // Per guidance on using AD test

static bool lowvariance(double mean, double stddev) {
  assert(stddev >= -1e-6);
  return (fabs(mean) < LOWMEAN_THRESHOLD)
    || (stddev < LOWSTDDEV_THRESHOLD);
}

//
// Produce a statistical summary (stored in 'm') over all runs.  Time
// values are single int64_t fields storing microseconds.
//
static void measure(Usage *usage,
		    int64_t start,
		    int64_t end,
		    FieldCode fc,
		    Comparator compare,
		    Measures *m) {
  int64_t runs = end - start;
  if (runs < 1) PANIC("No data to analyze");
  int64_t *X = ranked_sample(usage, start, end, fc, compare);

  // Descriptive statistics of the data distribution
  m->mode = estimate_mode(X, runs);
  m->min = percentile(0, X, runs);
  m->Q1 = percentile(25, X, runs);
  m->median = percentile(50, X, runs);
  m->Q3 = percentile(75, X, runs);
  m->pct95 = percentile(95, X, runs);
  m->pct99 = percentile(99, X, runs);
  m->max = percentile(100, X, runs);

  // Estimates based on the data distribution 
  m->est_mean = estimate_mean(X, runs);
  if (runs > 1)
    m->est_stddev = estimate_stddev(X, runs, m->est_mean);
  else
    m->est_stddev = 0;

  // Compute Anderson-Darling distance from normality if we have
  // enough data points.  The literature suggests that 8 suffices.
  // Also, if the estimated stddev is too low, the ADscore is not
  // meaningful (and numerically unstable) so we skip it.
  //
  // Skew also depends on having a variance that is not "too low".
  m->code = 0;
  if (runs < ADTEST_N_THRESHOLD)
    SET(m->code, CODE_SMALLN);
  if (lowvariance(m->est_mean, m->est_stddev))
    SET(m->code, CODE_LOWVARIANCE);

  assert(m->skew == 0.0);
  if (m->code != 0) goto noscore;

  m->skew = skew(X, runs, m->est_mean, m->est_stddev);
  if (nonnormal_skew(m->skew, runs))
    SET(m->code, CODE_HIGH_SKEW);
  
  m->kurtosis = kurtosis(X, runs, m->est_mean, m->est_stddev);
  if (nonnormal_kurtosis(m->kurtosis, runs))
    SET(m->code, CODE_HIGH_KURTOSIS);

  // Off-the-charts z-scores are detected when calculating the AD score.
  if (!AD_normality(X, runs, m->est_mean, m->est_stddev, &(m->ADscore))) {
    SET(m->code, CODE_HIGHZ);
    goto noscore;
  }
  m->p_normal = calculate_p(m->ADscore);
  free(X);
  return;

 noscore:
  m->p_normal = -1;
  free(X);
  return;
}

static Summary *new_summary(void) {
  // C99 says that zero, when cast to a pointer, is NULL, so fields
  // like 'cmd', 'shell', and 'infer' start out NULL.  And of course,
  // all the numeric fields are initialized to zero.
  Summary *s = calloc(1, sizeof(Summary));
  if (!s) PANIC_OOM();
  return s;
}

void free_summary(Summary *s) {
  if (!s) return;
  free(s->cmd);
  free(s->shell);
  free(s->name);
  if (s->infer) free(s->infer);
  free(s);
}

static Summary **new_summaries(int64_t n) {
  return calloc(n, sizeof(Summary *));
}

void free_summaries(Summary **ss, int64_t n) {
  if (!ss) return;
  for (int64_t i = 0; i < n; i++) {
    Summary *s = ss[i];
    free(s->cmd);
    free(s->shell);
    free(s->name);
    if (s->infer) free(s->infer);
    free(s);
  }
  free(ss);
}

// -----------------------------------------------------------------------------
// Compute statistical summary of a sample (collection of observations)
// -----------------------------------------------------------------------------

//
// Summarize from usage[start] to usage[end-1]
//
Summary *summarize(Usage *usage, int64_t start, int64_t end) {
  if (!usage) return NULL;
  if ((start < 0) || (end > usage->next)) return NULL;

  char *tmp;
  Summary *s = new_summary();
  s->cmd = strndup(get_string(usage, start, F_CMD), MAXCMDLEN);
  tmp = get_string(usage, start, F_SHELL);
  s->shell = tmp ? strndup(tmp, MAXCMDLEN) : NULL;
  tmp = get_string(usage, start, F_NAME);
  s->name = tmp ? strndup(tmp, MAXCMDLEN) : NULL;
  s->batch = usage->data[start].batch;
  s->runs = end - start;
  for (int64_t i = start; i < end; i++) 
    s->fail_count += (get_int64(usage, i, F_CODE) != 0);

  measure(usage, start, end, F_TOTAL, compare_totaltime, &s->total);
  measure(usage, start, end, F_USER, compare_usertime, &s->user);
  measure(usage, start, end, F_SYSTEM, compare_systemtime, &s->system);
  measure(usage, start, end, F_MAXRSS, compare_maxrss, &s->maxrss);
  measure(usage, start, end, F_VCSW, compare_vcsw, &s->vcsw);
  measure(usage, start, end, F_ICSW, compare_icsw, &s->icsw);
  measure(usage, start, end, F_TCSW, compare_tcsw, &s->tcsw);
  measure(usage, start, end, F_WALL, compare_wall, &s->wall);

  return s;
}

static Ranking *make_ranking(Usage *usage, int64_t start, int64_t end) {
  if (!usage) PANIC_NULL();
  if ((start < 0) || (end < 0) || (end <= start))
    PANIC("Start or end index is invalid (%" PRId64 ", %" PRId64 ")", start, end);

  int64_t maxsummaries = end - start;
  Ranking *rank = malloc(sizeof(Ranking));
  if (!rank) PANIC_OOM();
  rank->usage = usage;
  rank->usageidx = malloc((maxsummaries + 1) * sizeof(int64_t));
  if (!rank->usageidx) PANIC_OOM();
  //
  // Fill in the usageidx array:
  //   summary[i] is based on usage[j,k] where
  //   j = usageidx[i] and k = usageidx[i+1]
  //
  // Note that the end index k is exclusive.
  //
  int64_t count = 0;
  rank->usageidx[0] = 0;
  int64_t i = 0;
  while (i < end) {
    // Find usage index with a different batch number than the one at 'start'
    int64_t batch = usage->data[i].batch;
    for (i++; i < end; i++) 
      if (usage->data[i].batch != batch) break;
    rank->usageidx[count+1] = i;
    count++;
  }

  // Summarize each section of the usage array.  
  rank->summaries = new_summaries(count);
  for (i = 0; i < count; i++)
    rank->summaries[i] = summarize(usage,
				   rank->usageidx[i],
				   rank->usageidx[i+1]);

  rank->index = sort_by_totaltime(rank->summaries, 0, count);
  rank->count = count;
  return rank;
}

void free_ranking(Ranking *rank) {
  if (!rank) return;
  if (rank->usage)
    free_usage_array(rank->usage);
  if (rank->summaries)
    free_summaries(rank->summaries, rank->count);
  free(rank->index);
  free(rank->usageidx);
  free(rank);
}

Ranking *rank(Usage *usage) {
  if (!usage || (usage->next == 0)) return NULL; // No data
  
  Ranking *ranking = make_ranking(usage, 0, usage->next);
  if (ranking->count == 0) {
    free_ranking(ranking);
    return NULL;
  }

  int64_t start = 0;
  int64_t end = ranking->count;

  // Compute the comparative statistics between each sample and the
  // fastest one.
  int64_t bestidx = ranking->index[0];

  Summary **s = ranking->summaries;

  // If we are not printing the ranking, we don't compute it, because
  // it's expensive, needing O(n^2) time and space when there are n
  // observations in each of the two samples being compared.
  if (option.ranking) {
    // Need a minimum of INFERENCE_N_THRESHOLD observations
    if (s[bestidx]->runs >= INFERENCE_N_THRESHOLD) {
      // Compare each sample to the best performer
      for (int64_t k = start; k < end; k++) {
	int64_t i = ranking->index[k];
	if (i == bestidx) continue;
	s[i]->infer =
	  compare_samples(usage,
			  config.alpha,
			  ranking->usageidx[bestidx], ranking->usageidx[bestidx+1],
			  ranking->usageidx[i], ranking->usageidx[i+1]);
      }
    }
  }
  return ranking;
}

// -----------------------------------------------------------------------------
// Inferential statistics
// -----------------------------------------------------------------------------

// NOTE: The worst-case for counting differences is when all
// differences are the same.  There are n1*n2 differences.  If we
// limit the number of observations, n1 and n2, to UINT32_MAX, we can
// store a count of n1*n2 in uint64_t.

typedef struct RankedCombinedSample {
  int32_t  n1;	     // Size of sample 1
  int32_t  n2;	     // Size of sample 2
  int64_t  min;	     // Delta stored in count[0] 
  int64_t  sz;	     // Size of count array
  int64_t  zeros;    // Count of zeros, derived from count[]
  int64_t  pos;	     // Count of positive deltas, derived from count[]
  int64_t  neg;	     // Count of negative deltas, derived from count[]
  int64_t *count;    // Count (number of each delta value)
} RankedCombinedSample;

// How far must we right-shift 'val' so that it fits into 'bits' bits?
//
// Note: If 'val', an observed time duration in μs, reaches 60 bits,
// it's value is around 36,533 YEARS.  Imo, it's ok if we do not
// support measuring CPU times that large.
//
#define MAX_OBS_BITS 60
//
static uint8_t shift_count(int64_t val, const uint8_t bits) {
  assert(val > 0);
  uint8_t k;
  uint64_t uval = ((uint64_t) val) >> bits;
  for (k = 0; (k < MAX_OBS_BITS) && (uval >> k); k++);
  if (k < MAX_OBS_BITS) return k;
  PANIC("Observed value (%" PRId64 ") out of range (%u bits)",
	val, MAX_OBS_BITS);
}

#define MAX_DIFF_PRECISION 31
#define WARNING_DIFF_PRECISION 29
#define WARNING_DIFF_VALUE (((int64_t) 1) << WARNING_DIFF_PRECISION)
#define WARNING_MINUTES ((double) WARNING_DIFF_VALUE / (double) MICROSECS_PER_MIN)

#define WARNING_SAMPLE_DIFFERENCES ((int64_t) 40000 * (int64_t) 40000)

static RankedCombinedSample
rank_differences(Usage *usage,
		 int64_t start1, int64_t end1,
		 int64_t start2, int64_t end2,
		 FieldCode fc) {
  if (((end1 - start1) <= 0) || ((end2 - start2) <= 0))
    PANIC("Invalid sample sizes");

  if (((start1 >= start2) && (start1 < end2))
      || ((end1 > start2) && (end1 < end2)))
    PANIC("Invalid sample index ranges in usage structure");

  if (!FNUMERIC(fc)) PANIC("Invalid int64 field code (%d)", fc);

  int64_t n1 = end1 - start1;
  int64_t n2 = end2 - start2;
  int64_t N = n1 * n2;

  if (N > WARNING_SAMPLE_DIFFERENCES) {
    WARN("Number of sample differences is large (≈ %0.1f billion).\n",
	 (double) N / pow(10.0, 9));
    WARN("Ranking calculation will be slow.\n");
  }

  if (n1 > MAXRUNS)
    PANIC("Too many observations to rank (%" PRId64 "), limit is %u", n1, MAXRUNS);
  if (n2 > MAXRUNS)
    PANIC("Too many observations to rank (%" PRId64 "), limit is %u", n1, MAXRUNS);

  int metric_index = FTONUMERICIDX(fc);

  // Find the range of deltas between sample 1 and sample 2.
  int64_t least = INT64_MAX;
  int64_t most = INT64_MIN;
  for (int64_t i = 0; i < n1; i++) 
    for (int64_t j = 0; j < n2; j++) {
      int64_t obs1 = usage->data[start1 + i].metrics[metric_index];
      int64_t obs2 = usage->data[start2 + j].metrics[metric_index];
      int64_t delta = obs1 - obs2;
      least = (delta < least) ? delta : least;
      most = (delta > most) ? delta : most;
      assert(most >= least);
    }

  // The counting sort table is indexed by delta, from 0 to 'sz'.  If
  // this range fits into MAX_DIFF_PRECISION bits, we are good.
  // Otherwise, we scale the deltas (losing some precision).

  int64_t max_index = most - least;
  uint8_t shift = shift_count(max_index, MAX_DIFF_PRECISION);
  int64_t sz = (max_index >> shift) + 1;
  assert((sz >> MAX_DIFF_PRECISION) <= 1);

  if (sz >= WARNING_DIFF_VALUE) {
    WARN("Largest differences between observations exceeds "
	 "%0.1f minutes.\n", WARNING_MINUTES);
    WARN("Ranking calculation will be slow.\n");
  }
  if (shift) {
    WARN("Scaling down, forgoing %u bit%s of precision "
	 "in difference ranking.\n", shift, (shift == 1) ? "" : "s");
  }
  int64_t *count = calloc(sz, sizeof(uint64_t));
  if (!count) PANIC_OOM();
  for (int64_t i = 0; i < n1; i++) 
    for (int64_t j = 0; j < n2; j++) {
      int64_t obs1 = usage->data[start1 + i].metrics[metric_index];
      int64_t obs2 = usage->data[start2 + j].metrics[metric_index];
      int64_t idx = (obs1 - obs2 - least) >> shift;
      assert((idx >= 0) && (idx < sz));
      count[idx]++;
    }

  uint32_t zero_idx = (least > 0) ? 0 : -least;
  int64_t zeros = (least > 0) ? 0 : count[zero_idx];
  int64_t pos = 0, neg = 0;
  for (uint32_t i = 0; i < zero_idx; i++) {
    neg += count[i];
  }
  for (uint32_t i = zero_idx + (least <= 0); i < sz; i++) {
    pos += count[i];
  }

  if ((pos + zeros + neg) !=  N)
    PANIC("Total count (%" PRId64 " + %" PRId64 " + %" PRId64 ") != %" PRId64,
	  pos, zeros, neg, N);

  return (RankedCombinedSample)
    {.n1 = n1, .n2 = n2,
     .min = least,
     .sz = sz,
     .zeros = zeros, .pos = pos, .neg = neg,
     .count = count};
}

static double mann_whitney_U(RankedCombinedSample RCS) {
  double pos = RCS.pos;
  double neg = RCS.neg;
  return fmin(pos, neg) + 0.5 * (double) RCS.zeros;
}

// Sum of (t_k)^3 - t_k over all k, where t_k is the number of ties at
// rank k.
static double tie_correction(RankedCombinedSample RCS) {
  double correction = 0.0;
  double tk, amt;
  int64_t rank = 1;

  for (int64_t k = 0; k < RCS.sz; k++) {
    tk = (double) RCS.count[k];
    if (RCS.count[k] < 2) continue;
    amt = pow(tk, 3) - tk;
    if (DEBUG)
      DBG("Rank %" PRId64 " has a %.0f-way tie, contributing %0.2f\n",
	  rank, tk, amt);
    correction += amt;
    rank += RCS.count[k];
  }
  return correction;
}

// This calculation agrees with others' results, e.g.
// 
// https://www.statsdirect.co.uk/help/nonparametric_methods/mann_whitney.htm
// https://statisticsbyjim.com/hypothesis-testing/mann-whitney-u-test/
// https://support.minitab.com/en-us/minitab/help-and-how-to/statistics/nonparametrics/how-to/mann-whitney-test/methods-and-formulas/methods-and-formulas/
//
static double mann_whitney_p(RankedCombinedSample RCS, double U, double *adjustedp) {
  double n1 = RCS.n1;
  double n2 = RCS.n2;
  double n1n2 = n1 * n2;
  double n = n1 + n2;
  // Mean and std dev for U assumes U is normally distributed, which
  // it will be according to the theory
  double Umean = n1n2 / 2.0;
  // Continuity correction is 0.5, accounting for the fact that our
  // samples are not real numbered values of a function (which would
  // never produce a tie).
  double cc = (U > Umean) ? -0.5 : 0.5;
  double stddev = sqrt(n1n2 / 12.0 * (n + 1));
  double Z = (U - Umean + cc) / stddev;
  // Want probability that η₁ ≠ η₂
  double p = 2.0 * Phi(Z);
  // At high Z scores, calculating p with floating point may produce a
  // value outside of [0.0, 1.0].  So we clamp p to that range.
  if (p < 0.0) p = 0.0;
  if (p > 1.0) p = 1.0;
  if (DEBUG) DBG("Z = %0.2f\n", Z);
  if (adjustedp) {
    double factor = n1n2 / 12.0;
    double addend1 = n + 1.0;
    double addend2 = tie_correction(RCS) / (n * (n - 1.0));
    if (addend2 > addend1) {
      // Too many ties to make adjustment: signal this with -1 probability
      *adjustedp = -1.0;
    } else {
      double stddev_adjusted = sqrt(factor * (addend1 - addend2));
      double adjustedZ = (U - Umean + cc) / stddev_adjusted;
      *adjustedp = 2.0 * Phi(adjustedZ);
      // Clamp adjustedp in case of small floating point error
      if (*adjustedp < 0.0) *adjustedp = 0.0;
      if (*adjustedp > 1.0) *adjustedp = 1.0;
      if (DEBUG) DBG("adjusted Z = %0.2f\n", adjustedZ);
    }
  }
  return p;
}

// Probability of superiority: The probability that an observation
// from sample 1 is better (total time is less) than an observation
// from sample 2.  Take the count of how many times this occurred in
// the collection of all pairwise differences, and divide by the total
// number of pairwise differences.
static double superiority(RankedCombinedSample RCS) {
  double n1 = RCS.n1;
  double n2 = RCS.n2;
  return RCS.neg / (n1 * n2);
}

// https://www.statsdirect.co.uk/help/nonparametric_methods/mann_whitney.htm
// "When samples are large (either sample > 80 or both samples >30) a
// normal approximation is used for the hypothesis test and for the
// confidence interval."
//
// https://seismo.berkeley.edu/~kirchner/eps_120/Toolkits/Toolkit_08.pdf
// "If m and n are small (say, mn<100 or so), then other methods are
// necessary to estimate confidence limits.  See Helsel and Hirsch."
//
// Input: RCS contains signed ranked differences.
// Returns confidence level, e.g. 0.955 for 95.5%, and sets
// 'lowptr' and 'highptr' to the ends of the interval.
// 
// Note: The calculation below agress with minitab and whatever system
// statsdirect uses.  The CI differs a bit from that of "Statistics by
// Jim" as he reports a narrower range by 1 rank on the low side and 1
// rank on the high side.  Which approach is best?  Hard to say.
//
static double median_diff_ci(RankedCombinedSample RCS,
			     double alpha,
			     int64_t *lowptr,
			     int64_t *highptr) {
  double n1 = RCS.n1;
  double n2 = RCS.n2;
  double N = n1 * n2;
  double Zcrit = invPhi(1.0 - alpha/2.0);
  // Calculate ranks for each end of confidence interval
  double low_rank = (N / 2.0) - Zcrit * sqrt(N * (n1 + n2 + 1) / 12.0);
  low_rank = floor(low_rank);
  double high_rank = floor(N - low_rank + 1);

  double rank;
  int64_t count = 1;
  int64_t low = -1, high = -1;
  int64_t lowcount = -1, highcount = -1;

  for (int64_t k = 0; k < RCS.sz; k++) {
    if (RCS.count[k] == 0) continue;
    rank = count;
    if (RCS.count[k] > 1)
      rank += (double) 1.0 / (double) RCS.count[k];
    if ((low == -1) && (rank > low_rank)) {
      low = k;
      lowcount = count;
    }
    if (rank < high_rank) {
      high = k;
      highcount = count + RCS.count[k] - 1;
    }
    count += RCS.count[k];
  }

  if (low == -1) {
    low = 0;
    lowcount = 0;
  }
  if (high == -1) {
    high = RCS.sz;
    highcount = count;
  }

  low += RCS.min;
  high += RCS.min;

  *lowptr = low;
  *highptr = high;

  double ci_width = highcount - lowcount;
  double actual_Z = ci_width / 2.0 / sqrt(N * (n1 + n2 + 1) / 12.0);

  if (DEBUG) {
    DBG("low = %" PRId64 ", lowcount = %" PRId64
	", high = %" PRId64 ", highcount = %" PRId64 "\n", 
	low, lowcount, high, highcount);
    DBG("ci_width = %.2f, Z = %.2f\n", ci_width, actual_Z);
  }

  double confidence =  2.0 * Phi(actual_Z) - 1.0;
  return confidence;
}

// Hodges-Lehmann estimation of location shift, sometimes called the
// median difference, but that term is easily confused with the
// difference of the medians.
// 
// https://en.wikipedia.org/wiki/Hodges–Lehmann_estimator "The
// Hodges–Lehmann statistic is the median of the m × n differences"
// between two samples.  Per Wikipedia, citing for evidence Myles
// Hollander. Douglas A. Wolfe. Nonparametric statistical methods. 2nd
// ed. John Wiley:
//
// "It is a robust statistic that has a breakdown point of 0.29, which
// means that the statistic remains bounded even if nearly 30 percent
// of the data have been contaminated. This robustness is an important
// advantage over the sample mean, which has a zero breakdown point,
// being proportional to any single observation and so liable to being
// misled by even one outlier. The sample median is even more robust,
// having a breakdown point of 0.50."
//
static double median_diff_estimate(RankedCombinedSample RCS) {
  uint32_t total = RCS.pos + RCS.neg + RCS.zeros;
  uint32_t h = total / 2;
  if ((total % 2) == 1) h++;
  uint32_t idx = 0, next = 123456;
  uint32_t count = 0;

  for (; (idx < RCS.sz) && (count < h); idx++) count += RCS.count[idx];
  if (idx) idx--;

  if (DEBUG) {
    DBG("n1 = %u, n2 = %u, n1*n2 = %u, total = %u\n",
	RCS.n1, RCS.n2, RCS.n1 * RCS.n2, total);
    DBG("h = %u, count = %u, idx = %u, next = %u, RCS.min = %" PRId64 "\n",
	h, count, idx, next, RCS.min);
  }

  if ((total % 2) != 0)
    return (double) idx + (double) RCS.min;

  // Else break a tie

  // Is the next value in the same bucket?  Then we're done.
  if (RCS.count[idx] > 1) 
    return (double) idx + (double) RCS.min;

  // Else we need to find the next value in the count array
  next = (idx < RCS.sz) ? idx + 1 : RCS.sz - 1;
  while ((RCS.count[next] == 0) && (next < RCS.sz)) next++;
  return (((double) next + (double) idx) / 2.0) + (double) RCS.min;
}

// -----------------------------------------------------------------------------
// Misc
// -----------------------------------------------------------------------------

static int compare_median_total_time(const void *idx_ptr1,
				     const void *idx_ptr2,
				     void *context) {
  Summary **s = context;
  const int64_t idx1 = *((const int64_t *)idx_ptr1);
  const int64_t idx2 = *((const int64_t *)idx_ptr2);
  int64_t val1 = s[idx1]->total.median;
  int64_t val2 = s[idx2]->total.median;
  if (val1 > val2) return 1;
  if (val1 < val2) return -1;
  return 0;
}

// Caller must free the returned array
int64_t *sort_by_totaltime(Summary **summaries, int64_t start, int64_t end) {
  if (!summaries || !*summaries) PANIC_NULL();
  int64_t n = end - start;
  if (n < 1) return NULL;
  int64_t *index = malloc(n * sizeof(int64_t));
  if (!index) PANIC_OOM();
  for (int64_t i = 0; i < (end - start); i++) index[i] = i+start;
  sort(index, n, sizeof(int64_t), compare_median_total_time, summaries);
  return index;
}

// Returns NULL if there are insufficient observations in either
// sample to calculate inferences.
Inference *compare_samples(Usage *usage,
			   double alpha,
			   int64_t ref_start, int64_t ref_end,
			   int64_t idx_start, int64_t idx_end) {

  int64_t n1 = ref_end - ref_start;
  int64_t n2 = idx_end - idx_start;
  if ((n1 < INFERENCE_N_THRESHOLD)
      || (n2 < INFERENCE_N_THRESHOLD)) return NULL;
  
  Inference *stat = malloc(sizeof(Inference));
  if (!stat) PANIC_OOM();

  RankedCombinedSample RCS =
    rank_differences(usage,
		     idx_start, idx_end,
		     ref_start, ref_end,
		     F_TOTAL);
  stat->U = mann_whitney_U(RCS);
  stat->p = mann_whitney_p(RCS, stat->U, &(stat->p_adj));
  stat->p_super = superiority(RCS);

  stat->shift = median_diff_estimate(RCS);
  stat->confidence =
    median_diff_ci(RCS, alpha, &(stat->ci_low), &(stat->ci_high));

  stat->indistinct = 0;

  // Is the p value (or the adjusted one) not low enough?
  if (!(stat->p < alpha) || !(stat->p_adj < alpha))
    SET(stat->indistinct, INF_NONSIG);

  // Check for end of CI interval being too close to zero
  bool ci_touches_0 = ((llabs(stat->ci_low) <= config.epsilon) ||
		       (llabs(stat->ci_high) <= config.epsilon));
  // Or CI outright includes zero
  bool ci_includes_0 = (stat->ci_low < 0) && (stat->ci_high > 0);

  if (ci_touches_0 || ci_includes_0)
    SET(stat->indistinct, INF_CIZERO);

  // Check for median difference (effect size) too small
  if (fabs(stat->shift) < (double) config.effect) 
    SET(stat->indistinct, INF_NOEFFECT);

  if (stat->p_super > config.super)
    SET(stat->indistinct, INF_HIGHSUPER);

  free(RCS.count);
  return stat;
}
